# Ano Browser

Ano Browser is a safe High Speed Internet Browser! 

# Download

For Android:

Download at Google Play: https://play.google.com/store/apps/details?id=gigprojekt.com.webbrowerpro


For Windows:

Download as zip: https://gigserver.000webhostapp.com/setup.zip

# Is it safe?

VirusTotal Scan for Android Version: https://www.virustotal.com/#/file/9c90bbfbb807d369e9ef965d5df82b8780d91362f538d5d674408c197cf27d66/

VirusTotal Scan for Windows Version: https://www.virustotal.com/#/file/c5b1b5574eb3354aacf4c015606ef39724c804bc1596d04dcbaf8c8363d44426/

# Donate

PayPal: http://bit.ly/spendenforanobrowser




